export interface IQuery {
    confidenceLevel?: number;
    transactionId?: string;
}

export interface IGeoInfo {
    latitude: number;
    longitude: number;
}

export interface ITransaction {
    age: number;
    email: string;
    geoInfo: IGeoInfo;
    name: string;
    phone: string;
}

export interface IConnectionInfo {
    confidence: number;
    type: string;
}

export interface ICombinedConnectionInfo {
    confidence: number;
    type: string[];
}

export type IConnInfo<T> = T & {
    connectionInfo?: IConnectionInfo;
};

export type ITransactionFlat<T> = T & {
    id: string;
};

export type ITransactionGraph<T> = IConnInfo<ITransactionFlat<T>> & {
    children: Array<ITransactionGraph<T>>;
};

export type IData<T> = IConnInfo<ITransactionGraph<T>>;

export type IDataSet = Array<IData<ITransaction>>;

export type ICombinedConn<T> = T & {
    combinedConnectionInfo?: ICombinedConnectionInfo;
};

export type IResponse<T> = ICombinedConn<ITransactionFlat<T>>;
