import {
    ICombinedConn,
    ICombinedConnectionInfo,
    IData,
    IQuery,
    IResponse,
    ITransactionFlat,
    ITransactionGraph} from './../interface/data.interface';

/*
 * Service to get confident transactions list.
 * takes dataset and url query params
 */
export const getConfidentTransactionsBy = <T>(dataSet: Array<IData<T>>, {transactionId = '', confidenceLevel = 1}: IQuery): Array<IResponse<T>> => {
    // Find transaction by id in given dataSet
    const start = findT(dataSet, transactionId);
    // Get connectionInfo for base transaction
    const ci = start !== undefined && start.connectionInfo !== undefined ? start.connectionInfo : undefined;
    const connInfo = ci !== undefined ? {'type': [ci.type], 'confidence': ci.confidence} : undefined;

    /*
     * Create flat array of transactions from base transaction and
     * array of connected transactions with given level of confidence
     */
    return start !== undefined ? [removeProps(start, 'connectionInfo', 'children'),
                                  ...flattenChildren(start.children, confidenceLevel, connInfo)] : [];
};

/***
 * Flatten the list of child transaction, adding combinedConnection info to each of it
 */
const flattenChildren = <T>(transactions: Array<ITransactionGraph<T>> | undefined, minConfidence: number,
                            connInfo?: ICombinedConnectionInfo): Array<ICombinedConn<ITransactionFlat<T>>> => {

    const {confidence, type} = connInfo !== undefined ? connInfo : {'confidence': 1, 'type': []};

    if (transactions === undefined || transactions.length === 0) {
        return [];
    }

    // map child transactions recursively to nested array, then flatten it.
    return transactions.map((transaction) => {
            // calculate confidence level and type based on parent's value
        const conf = transaction.connectionInfo !== undefined ? confidence * transaction.connectionInfo.confidence : 0,
            // create connection type array as concatenation to parent's
            tp = transaction.connectionInfo !== undefined ? [transaction.connectionInfo.type, ...type] : [...type],
            combinedTransaction: ICombinedConn<ITransactionGraph<T>> = addCombinedConnection(transaction,
                    {
                        confidence: conf,
                        type: tp}),
            // remove 'children' property from transaction            
            childlessCombined = removeProps(combinedTransaction as ICombinedConn<ITransactionGraph<T>>, 'children');

        // recurse one level deeper, if confidence level for children still more, then requested
        return conf >= minConfidence ? [
                                        childlessCombined,
                                        ...flattenChildren(transaction.children, minConfidence, {confidence: conf, type: tp})
                                       ] : [];
    
            })
            // flatten resulting array
            .flat(Infinity);

};

/***
 * Search transaction in dataSet by id recursively
 */
const findT = <T>(data: Array<ITransactionGraph<T>> = [], id: string): ITransactionGraph<T> | undefined => {

    if (data.length > 0) {
        for (const t of data) {
            if (t.id === id) {
                return t;
            }
            const found = findT(t.children, id);
            if (found !== undefined) {
                return found;
            }
        }
    }
};

/***
 * Helper typesafe function to add combinedConnectionInfo to an object
 */
const addCombinedConnection = <T>(data: T, combinedConnectionInfo: ICombinedConnectionInfo): ICombinedConn<T> => 
    ({combinedConnectionInfo, ...data});

/***
 * Helper typesafe function to remove properties from an object 
 */
 const removeProps = <T, U>(transaction: T, ...props: string[]): U => Object.keys(transaction).
        reduce((object, key) => {

            if (!props.includes(key)) {
                (object as any)[key] = (transaction as any)[key];          
            }
            return object;
        }, {} as U);
