import fs from 'fs';
import util from 'util';
import {IDataSet} from './../interface/data.interface';

const readFile = util.promisify(fs.readFile);

/**
 * Service loading transaction dataset from json file.
 * Silently returns empty array on errors.
 */
export const getData = async (): Promise<IDataSet> => {
    const data = await readFile('./dataset/test.data.json');

    try {
        return JSON.parse(data.toString()) as IDataSet;
    } catch (err) {
        return [];
    }
};
