import express from 'express';
import http from "http";

import {IDataSet, IQuery} from "./interface/data.interface";
import {getConfidentTransactionsBy} from "./services/confidence";
import {getData} from "./services/data";

const DEFAULT_PORT = 3000,
    router = express(),
    {PORT = DEFAULT_PORT} = process.env,
    server = http.createServer(router);

let dataSet: IDataSet = [];

router.get("/api/transactions", (req, res) => {
    res.json(getConfidentTransactionsBy(dataSet, req.query as IQuery));
});

const _ = (async () => {

    dataSet = await getData();
    server.listen(
        PORT,
        () => {
            console.log(`server is  running http://localhost:${PORT}`);
        }
    );
})();
