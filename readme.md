# Node.js + Typescript + Express API Microservice demo.

A microservice built using Node.js and Typescript.

## Demo

http://212.90.42.166/api/transactions


## Installing / Getting started
   
```
npm install
npm run dev
```

## Prerequisites.

### Create the following API endpoint:

#### GET
    /api/transactions?transactionId={transactionId}&confidenceLevel={confidence}
The request should return a list of transactions, containing the transaction with the transactionId query param, along with all its children that have a connection confidence same or bigger than the confidenceLevel query param. This should be a flattened structure.

- sample request:
    http://212.90.42.166/api/transactions?transactionId=5c868b22eb7069b50c6d2d32&confidenceLevel=0.7

### Solution details

- Endpoint routing is implemented using Express router.
- All business logic is implemented as a set of functions.
- Every function is implemented as generic type, therefore transaction payload details are decoupled.
- Demo is hosted on IIS server using IISNode.

## Licensing

MIT